# Mailaustausch mit Axel Dürkop

Liebe Juliane,

vielen Dank für Dein Feedback. Wir haben eine GitPitch-Installation an
der TU, die nur Präsen aus dem GitLab an der TUHH zieht. Beim Aufsetzen
von GitPitch muss/kann das konfiguriert werden.

Mögliche Lösungen:

1) Du hostest Deine Präse im GitLab der TUHH.  
2) Du hostest sie bei GitHub und nutzt das offizielle GitPitch.  
3) Das RRZ setzt auch ein GitPitch auf und zeigt auf Euer GitLab.  
4) Du hostest die Präse im GitLab des RRZ und redundant auf GitHub, um
das offizielle GitPitch nutzen zu können. In den Settings Deines
GitLab-Repos stellst Du unter "Repository" das GitHub-Repo ein, sodass
immer automatisch auch dorthin gepusht wird.

Hoffe, das hilft weiter, sonst gern noch einmal melden.

Liebe Grüße
XL


Am 20.07.2018 um 11:09 schrieb Juliane Jacob:
>
> Lieber Axel,
>
> ich habe Anfang diesen Monats deine Einführung in Git besucht und wollte
> mich nochmal für den sehr informativen Kurs bedanken.
> Nun hab ich mir endlich GitPitch abgeguckt und eine Frage dazu.
> Vielleicht kannst du mir weiterhelfen?
> Ich habe ein neues Projekt "GitPitch" im RRZ-UHH-Gitlab angelegt und
> dort eine pitchme.md hinterlegt.
> Laut Anleitung müsste ich dann auf
> https://gitpitch.com/$user/$repo/$branch gehen und die Präsentation
> würde gestartet werden... Funktioniert aber nicht. Es wird "Invalid
> Slideshow Presentation URL" oder "Slideshow Presentation Not Found"
> angezeigt und ich weiß nicht, wo ich einen Fehler mache...
> Habe ich einen Schritt vergessen?
>
> Dann habe ich gesehen, dass der Link zu deiner Präsentation ganz anders
> lautet und sogar corporate design ist
> https://screen.rz.tuhh.de/2018-07-gitlab-workshop-haw/2018-07-gitlab-workshop-haw-presentation#/11
>
> Ist das so, weil euer RRZ gitpitch direkt anbietet?
>
> Vielen Dank für deine Hilfe und vieele Grüße
> Juliane
